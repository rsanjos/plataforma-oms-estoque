﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.WebApi
{
    public static class Utils
    {
        private const string CONFIGURATION_APPLICATION_ENVIRONMENT = "ApplicationEnvironment";

        public static ApplicationEnvironments GetApplicationEnvironment(IConfiguration configuration)
        {
            var value = configuration.GetSection(CONFIGURATION_APPLICATION_ENVIRONMENT).Value;
            if (string.IsNullOrEmpty(value))
                throw new System.InvalidOperationException($"Key \"{CONFIGURATION_APPLICATION_ENVIRONMENT}\" not found in configuration file");

            Enum.TryParse<ApplicationEnvironments>(value, true, out ApplicationEnvironments applicationEnvironment);
            if (applicationEnvironment == ApplicationEnvironments.None)
                throw new System.InvalidOperationException($"Key \"{CONFIGURATION_APPLICATION_ENVIRONMENT}\" in configuration file has a invalid value \"{value}\".");

            return applicationEnvironment;
        }
    }
}