﻿using Plataforma.Oms.Estoque.Application.Interfaces;
using Plataforma.Oms.Estoque.Application.Services;
using Plataforma.Oms.Estoque.Domain.Repositories;
using Plataforma.Oms.Estoque.Infrastructure.Data.Mongo;
using Plataforma.Oms.Estoque.Infrastructure.Data.Oracle;
using Plataforma.Oms.Estoque.Infrastructure.Interfaces;
using Plataforma.Oms.Estoque.Infrastructure.Repositories;

namespace Plataforma.Oms.Estoque.WebApi.Configurations
{
    public static class DependencyInjectionConfigurations
    {
        public static void AddApplicationSettings(this IServiceCollection services)
        {
            services.AddSingleton<IMongoSettings, MongoSettings>();
        }

        public static void AddApplicationRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IMongoDataAccess, MongoDataAccess>();
            services.AddSingleton<IOracleDataAccess, OracleDataAccess>();
            services.AddSingleton<IInventoryRepository, InventoryRepository>();
        }

        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddSingleton<IInventoryService, InventoryService>();
        }
    }
}