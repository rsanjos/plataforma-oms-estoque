﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using System.Diagnostics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.WebApi.Configurations
{
    public static class LoggingConfiguration
    {
        public static void AddCustomLogger(this IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ICustomLogger, CustomLogger>();
        }

        private class CustomLogger : CustomBaseLogger
        {
            private readonly ILogger logger;
            private readonly IConfiguration configuration;
            private readonly IHttpContextAccessor httpContextAccessor;
            private readonly JsonSerializerOptions jsonSerializerOptions;

            public CustomLogger(
                ILoggerFactory loggerFactory,
                IConfiguration configuration,
                IHttpContextAccessor httpContextAccessor)
                : base(Startup.APPLICATION_NAME)
            {
                this.logger = loggerFactory.CreateLogger<Startup>();
                this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
                this.httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));

                this.jsonSerializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web)
                {
                    DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
                };
                jsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            }

            public override void Log(ILog log, CustomLogLevels customLogLevel)
            {
                Debug.WriteLine($"LOG {log.MessageType}: {log.Message} {(log.ElapsedTimeMilliseconds.HasValue ? $"({log.ElapsedTimeMilliseconds}ms) " : "")}{log.Details}");

                var json = JsonSerializer.Serialize(log, jsonSerializerOptions);
                this.logger.Log((LogLevel)(int)customLogLevel, json);
            }
        }
    }
}
