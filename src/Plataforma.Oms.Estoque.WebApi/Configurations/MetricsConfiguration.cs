﻿using Prometheus.DotNetRuntime;

namespace Plataforma.Oms.Estoque.WebApi.Configurations
{
    public static class MetricsConfiguration
    {
        public static void AddPrometheus(this IConfigurationBuilder config)
        {
            IDisposable collector = DotNetRuntimeStatsBuilder
                .Customize()
                .WithContentionStats()
                .WithJitStats()
                .WithThreadPoolStats()
                .WithGcStats()
                .WithExceptionStats()
                .StartCollecting();
        }
    }
}
