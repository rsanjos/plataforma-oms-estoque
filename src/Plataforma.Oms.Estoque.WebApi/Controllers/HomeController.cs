﻿using Microsoft.AspNetCore.Mvc;

namespace Plataforma.Oms.Estoque.WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    [Route("")]
    public class HomeController : ControllerBase
    {
        public IActionResult Index()
        {
            return Redirect("/swagger/index.html");
        }
    }
}
