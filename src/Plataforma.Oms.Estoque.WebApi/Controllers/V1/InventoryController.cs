﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Plataforma.Oms.Estoque.Application.Interfaces;
using Plataforma.Oms.Estoque.Application.ViewModels;
using Plataforma.Oms.Estoque.Application.ViewModels.Inventory;
using Plataforma.Oms.Estoque.Application.ViewModels.Request;
using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.WebApi.Extensions;
using System.Net;

namespace Plataforma.Oms.Estoque.WebApi.Controllers.V1
{
    [ApiController]
    [ApiVersion("1")]
    [Route("v{version:apiVersion}/inventorys")]
    [Authorize]
    public class InventoryController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly ApplicationEnvironments environment;
        private readonly IInventoryService inventoryService;

        public InventoryController(
             IConfiguration configuration,
             IInventoryService inventoryService)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            this.environment = Utils.GetApplicationEnvironment(configuration);
            this.inventoryService = inventoryService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(InventoryResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(BadRequestResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetInventoryAsync(ProductRequestViewModel product)
        {
            var request = new InventoryFilterRequest(product.StoreReference, product.Product.Attributes.Sku);
            this.ProcessRequest(environment, request);
            var response = await inventoryService.GetInventoryAsync(request);
            return this.GetHttpResponse(environment, response, HttpStatusCode.OK);
        }
    }
}
