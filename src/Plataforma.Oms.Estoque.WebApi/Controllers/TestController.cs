﻿using Microsoft.AspNetCore.Mvc;
using Plataforma.Oms.Estoque.Application.ViewModels;
using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.WebApi.Extensions;

namespace Plataforma.Oms.Estoque.WebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    [Route("test")]
    public class TestController : ControllerBase
    {
        private readonly ApplicationEnvironments environment;
        private readonly ICustomLogger customLogger;

        public TestController(
            IConfiguration configuration,
            ICustomLogger customLogger)
        {
            this.environment = Utils.GetApplicationEnvironment(configuration);
            this.customLogger = customLogger;
        }

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            var response = new BaseResponse();
            response.AddNotification(NotificationTypes.Information, $"OK {DateTime.Now:yyyy-MM-dd HH:mm:ss}");
            return this.GetHttpResponse(environment, response);
        }

        [HttpGet]
        [Route("error")]
        public IActionResult Error()
        {
            throw new System.InvalidOperationException("Test Error");
        }

        [HttpGet]
        [Route("log")]
        public IActionResult Log()
        {
            var requestInfo = HttpContext.GetRequestInfo(environment);
            this.customLogger.LogInformation(requestInfo, "Log Teste");

            var response = new BaseResponse();
            response.AddNotification(NotificationTypes.Information, $"Log written {DateTime.Now:yyyy-MM-dd HH:mm:ss}");
            return this.GetHttpResponse(environment, response);
        }
    }
}