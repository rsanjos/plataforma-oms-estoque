using Plataforma.Oms.Estoque.WebApi;
using Plataforma.Oms.Estoque.WebApi.Configurations;

public class Program
{
    public static void Main(string[] args)
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR");

        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
             .ConfigureAppConfiguration((context, config) =>
             {
                 config.AddJsonFile("config/appsettings.json", optional: true, reloadOnChange: true);
                 var configuration = config.Build();
                 config.AddPrometheus();
             })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
}