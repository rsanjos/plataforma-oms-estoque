﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Plataforma.Oms.Estoque.WebApi.Configurations;
using ServicesSales.DigitalProductsRepo.WebApi.Configurations;

namespace Plataforma.Oms.Estoque.WebApi
{
    public class Startup
    {
        public const string APPLICATION_NAME = "Plataforma.Oms.Estoque.WebApi";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApiConfiguration(Configuration);
            services.AddSwaggerConfiguration();
            services.AddCustomLogger();
            services.AddAutoMapperConfiguration();
            services.AddApplicationSettings();
            services.AddApplicationRepositories();
            services.AddApplicationServices();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            app.UseApiConfiguration(env);
            app.UseSwaggerConfiguration(apiVersionDescriptionProvider);
        }
    }
}
