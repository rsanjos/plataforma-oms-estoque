﻿using Microsoft.AspNetCore.Http.Extensions;
using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using System.Text.Json;

namespace Plataforma.Oms.Estoque.WebApi.Extensions
{
    public static class HttpContextExtensions
    {
        public const string HEADER_CORRELATION_ID = "X-Correlation-ID";
        public const string HEADER_TENANT_ID = "Tenant-ID";
        public const string CONTEXT_ITEM_REQUEST_INFO = "RequestInfo";

        private static string GetHeaderValue(this HttpContext context, string headerName, string defaultValue)
        {
            if (context.Request.Headers.ContainsKey(headerName))
                return context.Request.Headers[headerName].ToString();

            return defaultValue;
        }

        private static string GetContextValue(this HttpContext context, string name, string defaultValue)
        {
            if (context.Items.ContainsKey(name))
                return context.Items[name].ToString();

            return defaultValue;
        }

        private static void SetContextValue(this HttpContext context, string name, string value)
        {
            context.Items.Add(name, value);
        }

        private static string GetSetContextValue(this HttpContext context, string name, string defaultValue)
        {
            if (context.Items.ContainsKey(name))
                return context.Items[name].ToString();

            context.Items.Add(name, defaultValue);
            return defaultValue;
        }

        public static Guid GetCorrelationId(this HttpContext context)
        {
            if (Guid.TryParse(GetHeaderValue(context, HEADER_CORRELATION_ID, null), out Guid guid))
                return guid;

            return Guid.NewGuid();
        }

        public static string GetTenantId(this HttpContext contex)
        {
            return GetHeaderValue(contex, HEADER_TENANT_ID, null);
        }

        public static string GetCurrentIp(this HttpContext context)
        {
            var ip = context.Connection.RemoteIpAddress?.ToString();

            if (string.IsNullOrEmpty(ip) || ip.Equals("::1"))
                return "localhost";

            return ip;
        }

        public static string GetUserAgent(this HttpContext context)
        {
            if (!context.Request.Headers.ContainsKey("User-Agent"))
                return null;

            return context.Request.Headers["User-Agent"];
        }

        public static IRequestInfo GetRequestInfo(this HttpContext context, ApplicationEnvironments environment)
        {
            var json = GetContextValue(context, CONTEXT_ITEM_REQUEST_INFO, null);
            if (json != null)
                return JsonSerializer.Deserialize<RequestInfo>(json);

            var requestInfo = new RequestInfo(
                correlationId: context.GetCorrelationId(),
                applicationEnvironment: environment,
                url: context.Request?.GetDisplayUrl(),
                httpVerb: context.Request?.Method,
                ip: context.GetCurrentIp(),
                userAgent: context.GetUserAgent(),
                machineName: Environment.MachineName,
                tenantId: context.GetTenantId()
            );

            json = JsonSerializer.Serialize(requestInfo);
            SetContextValue(context, CONTEXT_ITEM_REQUEST_INFO, json);
            
            return requestInfo;
        }
    }
}
