﻿using Microsoft.AspNetCore.Mvc;
using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.ViewModels;

namespace Plataforma.Oms.Estoque.WebApi.Extensions
{
    public static class ControllerExtensions
    {
        public static void ProcessRequest(this ControllerBase controller, ApplicationEnvironments environment, IRequest request)
        {
            if (request == null)
                return;

            request.RequestInfo = controller.HttpContext.GetRequestInfo(environment);
            request.TenantId = controller.HttpContext.GetTenantId();
        }

        public static ActionResult GetHttpResponse(this ControllerBase controller,
            ApplicationEnvironments environment,
            IResponse response,
            System.Net.HttpStatusCode successStatus = System.Net.HttpStatusCode.OK)
        {
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            var requestInfo = controller.HttpContext.GetRequestInfo(environment);

            response.AddCorrelationHeader(requestInfo.CorrelationId);

            var headers = response.GetHeaders();
            foreach (var header in headers)
                controller.Response.Headers.Add(header.Key, header.Value);

            if (response.Notifications != null)
            {
                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.InternalServerError))
                    return controller.StatusCode((int)System.Net.HttpStatusCode.InternalServerError, response);

                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.ServiceUnavailableError))
                    return controller.StatusCode((int)System.Net.HttpStatusCode.ServiceUnavailable, response);

                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.GatewayTimeout))
                    return controller.StatusCode((int)System.Net.HttpStatusCode.GatewayTimeout, response);

                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.UnauthorizedError))
                    return controller.StatusCode((int)System.Net.HttpStatusCode.Unauthorized, response);

                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.ForbiddenError))
                    return controller.StatusCode((int)System.Net.HttpStatusCode.Forbidden, response);

                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.ConflitError))
                    return controller.StatusCode((int)System.Net.HttpStatusCode.Conflict, response);

                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.NotFoundError))
                    return new NotFoundObjectResult(response);

                if (response.Notifications.Any(a => a.NotificationType == NotificationTypes.BadRequestError))
                    return new BadRequestObjectResult(response);

                if (response.Notifications.Any(a => a.IsError()))
                    return new BadRequestObjectResult(response);
            }

            return controller.StatusCode((int)successStatus, response);
        }
    }
}
