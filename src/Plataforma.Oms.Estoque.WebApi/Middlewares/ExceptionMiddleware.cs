﻿using Plataforma.Oms.Estoque.Application.ViewModels;
using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.WebApi.Configurations;
using Plataforma.Oms.Estoque.WebApi.Extensions;
using System.Diagnostics;
using System.Net;
using System.Text.Json;

namespace Plataforma.Oms.Estoque.WebApi.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ICustomLogger customLogger;
        private readonly ApplicationEnvironments environment;

        public ExceptionMiddleware(
            RequestDelegate next,
            ICustomLogger customLogger,
            IConfiguration configuration)
        {
            this.next = next;
            this.customLogger = customLogger;
            this.environment = Utils.GetApplicationEnvironment(configuration);
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                try
                {
                    var requestInfo = context.GetRequestInfo(environment);
                    this.customLogger.LogError(requestInfo, $"exception unhandled {ex.Message}", ex);
                }
                catch (Exception ex2)
                { 
                    System.Diagnostics.Debug.WriteLine(ex2);
                }

                var response = new InternalServerErrorResponse(ex.Message);
                await HandleRequestExceptionAsync(context, HttpStatusCode.InternalServerError, response);
            }
        }

        private static async Task HandleRequestExceptionAsync(HttpContext context, HttpStatusCode statusCode, BaseResponse result)
        {
            Debug.WriteLine($"ERROR! WebApi.ExceptionMiddleware -- Result: {JsonSerializer.Serialize(result)}");

            await context.Response.WriteAsJsonAsync(result, ApiConfiguration.GetJsonSerializerOptions());
            context.Response.StatusCode = (int)statusCode;
        }
    }
}
