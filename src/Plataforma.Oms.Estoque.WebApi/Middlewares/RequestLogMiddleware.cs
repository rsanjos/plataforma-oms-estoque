﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.WebApi.Extensions;

namespace Plataforma.Oms.Estoque.WebApi.Middlewares
{
    public class RequestLogMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ICustomLogger customLogger;
        private DateTime startDate;
        private readonly ApplicationEnvironments environment;

        public RequestLogMiddleware(RequestDelegate next,
            ICustomLogger customLogger,
            IConfiguration configuration)
        {
            this.next = next;
            this.customLogger = customLogger;
            this.environment = Utils.GetApplicationEnvironment(configuration);
        }

        public async Task Invoke(HttpContext context)
        {
            var ignorePaths = new string[] { "/actuator/health", "/readiness", "/metrics" };

            if (ignorePaths.Any(a => context.Request.Path.ToString().StartsWith(a, StringComparison.CurrentCultureIgnoreCase)))
            {
                await next(context);
                return;
            }

            this.startDate = DateTime.Now;

            await WriteLogRequestStart(context);

            var originalBodyStream = context.Response.Body;
            using var responseBody = new MemoryStream();
            context.Response.Body = responseBody;

            await next(context);

            await WriteLogRequestEnd(context);
            await responseBody.CopyToAsync(originalBodyStream);
        }

        private async Task WriteLogRequestStart(HttpContext context)
        {
            context.Request.EnableBuffering();
            var requestContent = await new StreamReader(context.Request.Body).ReadToEndAsync();
            context.Request.Body.Seek(0, SeekOrigin.Begin);

            var requestInfo = context.GetRequestInfo(environment);

            this.customLogger.LogRequestStart(requestInfo, requestContent);
        }

        private async Task WriteLogRequestEnd(HttpContext context)
        {
            context.Response.Body.Seek(0, SeekOrigin.Begin);
            string responseContent = await new StreamReader(context.Response.Body).ReadToEndAsync();
            context.Response.Body.Seek(0, SeekOrigin.Begin);

            var requestInfo = context.GetRequestInfo(environment);

            this.customLogger.LogRequestEnd(requestInfo, context.Response.StatusCode, DateTime.Now.Subtract(startDate).TotalMilliseconds, responseContent);
        }
    }
}