﻿namespace Plataforma.Oms.Estoque.Domain.Aggregates
{
    public class InventoryProduct
    {
        public string StoreReference { get; set; }
        public string Upsert { get; set; }
        public string OthersOutOfStock { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string PhotosUrls { get; set; }
        public string Unit { get; set; }
        public string MyProperty { get; set; }
        public string Price { get; set; }
        public string ClickMultiplier { get; set; }
        public string SpecialMaxQty { get; set; }
        public string ShowSubUnit { get; set; }
        public string SpecialPrice { get; set; }
        public string SubQty { get; set; }
        public string SubUnit { get; set; }
        public string MaxQty { get; set; }
        public long Stock { get; set; }
        public string EAN { get; set; }
        public string Boost { get; set; }
        public string Location { get; set; }
        public string SearchKeywords { get; set; }
        public string Brand { get; set; }
        public string Slug { get; set; }
        public string CategoryReference1 { get; set; }
        public string CategoryReference2 { get; set; }
        public string CategoryReference3 { get; set; }
        public string Format { get; set; }
        public string Equivalence { get; set; }
        public string UnitEquivalence { get; set; }
        public DateTime DateRegister { get; set; }
        public DateTime DateAlter { get; set; }

        public InventoryProduct(
            string storeReference,
            string upsert,
            string othersOutOfStock,
            string name,
            string sku,
            string photoUrls,
            string unit,
            string myProperty,
            string price,
            string clickMultiplier,
            string specialMaxQty,
            string showSubUnit,
            string specialPrice,
            string subQty,
            string subUnit,
            string maxQty,
            long stock,
            string ean,
            string boost,
            string location,
            string searchKeywords,
            string brand,
            string slug,
            string categoryReference1,
            string categoryReference2,
            string categoryReference3,
            string format,
            string equivalence,
            string unitEquivalence,
            DateTime dateRegister,
            DateTime dateAlter
            )
        {
            this.StoreReference = storeReference;
            this.Upsert = upsert;
            this.OthersOutOfStock = othersOutOfStock;
            this.Name = name;
            this.Sku = sku;
            this.PhotosUrls = photoUrls;
            this.Unit = unit;
            this.MyProperty = myProperty;
            this.Price = price;
            this.ClickMultiplier = clickMultiplier;
            this.SpecialMaxQty = specialMaxQty;
            this.ShowSubUnit = showSubUnit;
            this.SpecialPrice = specialPrice;
            this.SubQty = subQty;
            this.SubUnit = subUnit;
            this.MaxQty = maxQty;
            this.Stock = stock;
            this.EAN = ean;
            this.Boost = boost;
            this.Location = location;
            this.SearchKeywords = searchKeywords;
            this.Brand = brand;
            this.Slug = slug;
            this.CategoryReference1 = categoryReference1;
            this.CategoryReference2 = categoryReference2;
            this.CategoryReference3 = categoryReference3;
            this.Format = format;
            this.Equivalence = equivalence;
            this.UnitEquivalence = unitEquivalence;
            this.DateRegister = dateRegister;
            this.DateAlter = dateAlter;
        }
    }
}