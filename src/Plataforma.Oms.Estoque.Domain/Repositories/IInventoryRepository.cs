﻿using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.Domain.Aggregates;

namespace Plataforma.Oms.Estoque.Domain.Repositories
{
    public interface IInventoryRepository
    {
        Task<InventoryProduct> GetInventoryAsync(IRequestInfo requestInfo, string storeNumber, string productNumber);
    }
}