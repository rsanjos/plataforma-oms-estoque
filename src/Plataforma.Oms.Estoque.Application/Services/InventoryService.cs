﻿using Plataforma.Oms.Estoque.Application.Interfaces;
using Plataforma.Oms.Estoque.Application.Validators;
using Plataforma.Oms.Estoque.Application.ViewModels.Inventory;
using Plataforma.Oms.Estoque.Domain.Repositories;

namespace Plataforma.Oms.Estoque.Application.Services
{
    public class InventoryService : IInventoryService
    {
        private readonly IInventoryRepository inventoryRepository;

        public InventoryService(IInventoryRepository inventoryRepository)
        {
            this.inventoryRepository = inventoryRepository;
        }

        public async Task<InventoryResponse> GetInventoryAsync(InventoryFilterRequest request)
        {
            var ret = new InventoryResponse();
            ret.AddFluentNotifications(new InvetoryFilterRequestValidator().Validate(request));
            if (ret.HasErrors())
                return ret;

            var invetory = await this.inventoryRepository.GetInventoryAsync(request.RequestInfo, request.StoreNumber, request.ProductNumber);
            if (invetory == null)
            {
                ret.AddNotification(CrossCutting.Enum.NotificationTypes.NotFoundError, $"produto {request.ProductNumber} para a loja {request.StoreNumber} não encontrado");
                return ret;
            }

            ret = new InventoryResponse(
                new List<Model.InvetoryProductModel>()
                {
                    new Model.InvetoryProductModel("Total", invetory.Unit, invetory.Stock, invetory.DateAlter)
                });

            return ret;
        }
    }
}