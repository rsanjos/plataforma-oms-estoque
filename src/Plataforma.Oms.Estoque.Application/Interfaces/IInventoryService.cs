﻿using Plataforma.Oms.Estoque.Application.ViewModels.Inventory;

namespace Plataforma.Oms.Estoque.Application.Interfaces
{
    public interface IInventoryService
    {
        Task<InventoryResponse> GetInventoryAsync(InventoryFilterRequest request);
    }
}