﻿using FluentValidation;
using Plataforma.Oms.Estoque.Application.ViewModels.Inventory;
using Plataforma.Oms.Estoque.CrossCutting.Validators;

namespace Plataforma.Oms.Estoque.Application.Validators
{
    public class InvetoryFilterRequestValidator : AbstractValidator<InventoryFilterRequest>
    {
        public InvetoryFilterRequestValidator()
        {
            RuleFor(x => x).NotNull();
            //RuleFor(x => x.TenantId).NotEmpty().ValidTenantId();
            RuleFor(x => x.StoreNumber).NotEmpty();
            RuleFor(x => x.ProductNumber).NotEmpty();
        }
    }
}
