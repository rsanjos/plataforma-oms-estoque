﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class PresentationViewModel
    {
        [JsonPropertyName("price")]
        public PriceViewModel Price { get; set; }

        [JsonPropertyName("sub_quantity")]
        public int SubQuantity { get; set; }

        [JsonPropertyName("sub_unit")]
        public string SubUnit { get; set; }

        [JsonPropertyName("unit")]
        public string Unit { get; set; }

        [JsonPropertyName("volume")]
        public string Volume { get; set; }

        [JsonPropertyName("weight")]
        public string Weight { get; set; }
    }
}
