﻿using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class ProductViewModel
    {
        [JsonPropertyName("attributes")]
        public AttributesViewModel Attributes { get; set; }

        [JsonPropertyName("barcodes")]
        public List<string> Barcodes { get; set; }

        [JsonPropertyName("comment")]
        public string Comment { get; set; }

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("location")]
        public string Location { get; set; }

        [JsonPropertyName("max_allowed_found_quantity")]
        public double MaxAllowedFoundQuantity { get; set; }

        [JsonPropertyName("min_allowed_found_quantity")]
        public double MinAllowedFoundQuantity { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("photo_url")]
        public string PhotoUrl { get; set; }

        [JsonPropertyName("picking_index")]
        public int PickingIndex { get; set; }

        [JsonPropertyName("presentation")]
        public PresentationViewModel Presentation { get; set; }

        [JsonPropertyName("quantity")]
        public int Quantity { get; set; }

        [JsonPropertyName("quantity_found")]
        public int QuantityFound { get; set; }

        [JsonPropertyName("reference")]
        public string Reference { get; set; }

        [JsonPropertyName("state")]
        public StateViewModel State { get; set; }

        [JsonPropertyName("suggested_package")]
        public string SuggestedPackage { get; set; }

        [JsonPropertyName("task_info")]
        public TaskInfoViewModel TaskInfo { get; set; }
    }
}
