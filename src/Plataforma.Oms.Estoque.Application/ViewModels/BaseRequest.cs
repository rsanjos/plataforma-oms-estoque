﻿using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.CrossCutting.ViewModels;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class BaseRequest : IRequest
    {
        public string TenantId { get; set; }
        public IRequestInfo RequestInfo { get; set; }
    }
}
