﻿namespace Plataforma.Oms.Estoque.Application.ViewModels.Inventory
{
    public class InventoryFilterRequest : BaseRequest
    {
        public string? StoreNumber { get; private set; }
        public string? ProductNumber { get; private set; }

        public InventoryFilterRequest(string storeNumber, string productNumber)
        {
            StoreNumber = storeNumber;
            ProductNumber = productNumber;
        }
    }
}