﻿using Plataforma.Oms.Estoque.Application.Model;
using Plataforma.Oms.Estoque.CrossCutting.Domain;

namespace Plataforma.Oms.Estoque.Application.ViewModels.Inventory
{
    public class InventoryResponse : BaseResponse
    {
        public List<InvetoryProductModel> Data { get; set; }

        public InventoryResponse() : base()
        {

        }

        public InventoryResponse(List<Notification> notifications)
            : base(notifications)
        {

        }

        public InventoryResponse(List<InvetoryProductModel> data)
        {
            Data = data;
        }
    }
}