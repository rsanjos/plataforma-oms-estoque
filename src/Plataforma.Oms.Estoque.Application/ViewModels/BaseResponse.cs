﻿using FluentValidation.Results;
using Plataforma.Oms.Estoque.CrossCutting.Domain;
using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.ViewModels;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class BaseResponse : IResponse
    {
        public List<Notification> Notifications { get; set; }

        private readonly Dictionary<string, string> headers = new Dictionary<string, string>();

        public BaseResponse()
        {

        }

        public BaseResponse(List<Notification> notifications)
        {
            this.AddNotification(notifications);
        }

        #region Notifications

        public void AddNotification(NotificationTypes notificationType, string message)
        {
            AddNotification(new Notification(notificationType, message));
        }

        public void AddNotification(Notification notification)
        {
            if (this.Notifications == null)
                this.Notifications = new List<Notification>();

            this.Notifications.Add(notification);
        }

        public void AddNotification(List<Notification> notifications)
        {
            if (notifications == null)
                return;

            foreach (var notification in notifications)
                this.AddNotification(notification);
        }

        public void AddFluentNotifications(ValidationResult result)
        {
            foreach (var error in result.Errors)
                this.AddNotification(NotificationTypes.BadRequestError, error.ErrorMessage);
        }

        public bool HasErrors()
        {
            return (this.Notifications != null && this.Notifications.Any(a => a.IsError()));
        }

        public string GetErrorMessage()
        {
            return string.Join(", ", this.Notifications.Where(a => a.IsError()).Select(a => a.Message));
        }

        #endregion

        #region Headers

        public void AddHeader(string key, string value)
        {
            this.headers.Add(key, value);
        }

        public IDictionary<string, string> GetHeaders()
        {
            return this.headers;
        }

        public void AddCorrelationHeader(Guid correlationId)
        {
            this.AddHeader("X-Correlation-ID", correlationId.ToString());
        }

        #endregion
    }
}