﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class BadRequestResponse : BaseResponse
    {
        public BadRequestResponse(string message)
        {
            this.AddNotification(NotificationTypes.BadRequestError, message);
        }
    }
}
