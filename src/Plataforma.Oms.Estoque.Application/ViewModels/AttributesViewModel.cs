﻿using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class AttributesViewModel
    {
        [JsonPropertyName("ean")]
        public string EAN { get; set; }

        [JsonPropertyName("product_id")]
        public string ProductId { get; set; }

        [JsonPropertyName("location")]
        public string Location { get; set; }

        [JsonPropertyName("category")]
        public string Category { get; set; }

        [JsonPropertyName("sku")]
        public string Sku { get; set; }
    }
}
