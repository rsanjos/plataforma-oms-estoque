﻿using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class TaskInfoViewModel
    {
        [JsonPropertyName("departments")]
        public List<object> Departments { get; set; }

        [JsonPropertyName("id")]
        public string Id { get; set; }
    }
}
