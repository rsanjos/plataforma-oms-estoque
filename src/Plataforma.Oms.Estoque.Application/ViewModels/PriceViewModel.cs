﻿using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class PriceViewModel
    {
        [JsonPropertyName("amount")]
        public double Amount { get; set; }

        [JsonPropertyName("currency_code")]
        public string CurrencyCode { get; set; }
    }

}
