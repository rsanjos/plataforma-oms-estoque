﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.Application.ViewModels
{
    public class InternalServerErrorResponse : BaseResponse
    {
        public InternalServerErrorResponse(string message)
        {
            this.AddNotification(NotificationTypes.InternalServerError, message);
        }
    }
}
