﻿using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.Application.ViewModels.Request
{
    public class ProductRequestViewModel
    {
        [JsonPropertyName("product")]
        public ProductViewModel Product { get; set; }

        [JsonPropertyName("store_reference")]
        public string StoreReference { get; set; }
    }
}
