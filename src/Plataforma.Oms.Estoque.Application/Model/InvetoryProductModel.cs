﻿using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.Application.Model
{
    public class InvetoryProductModel
    {
        [JsonPropertyName("place_name")]
        public string PlaceName { get; private set; }

        [JsonPropertyName("unit")]
        public string Unit { get; private set; }

        [JsonPropertyName("quantity")]
        public long Quantity { get; private set; }
        
        [JsonPropertyName("updated_at")]
        public DateTime UpdatedAt { get; private set; }

        public InvetoryProductModel(string placeName, string unit, long quantity, DateTime updateAt)
        {
            this.PlaceName = placeName;
            this.Unit = unit;
            this.Quantity = quantity;
            this.UpdatedAt = updateAt;
        }
    }
}