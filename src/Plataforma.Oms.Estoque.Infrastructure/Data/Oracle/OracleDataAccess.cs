﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.Infrastructure.Interfaces;
using System.Data;
using System.Data.Common;
using System.Diagnostics;

namespace Plataforma.Oms.Estoque.Infrastructure.Data.Oracle
{
    public class OracleDataAccess : IOracleDataAccess
    {
        private readonly IConfiguration configuration;
        private readonly ICustomLogger customLogger;

        public OracleDataAccess(
            IConfiguration configuration,
            ICustomLogger customLogger
            )
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            this.customLogger = customLogger ?? throw new ArgumentNullException(nameof(customLogger));
        }

        public async Task<int> ExecuteAsync(
            IRequestInfo requestInfo, 
            OracleConnectionNames oracleConnectionName, 
            string sql, 
            DynamicParameters? parameters = null, 
            CommandType commandType = CommandType.StoredProcedure, 
            int commandTimeout = 15, 
            IDbTransaction? transaction = null)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using var connection = new OracleConnection(GetConnectionString(oracleConnectionName));
                await connection.OpenAsync();

                var ret = await connection.ExecuteAsync(
                    sql: sql,
                    param: parameters,
                    commandType: commandType,
                    commandTimeout: commandTimeout,
                    transaction: transaction);

                LogSuccess(requestInfo, sql, parameters, stopwatch);

                return ret;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, sql, parameters, stopwatch);
            }
        }

        public async Task<DbDataReader> ExecuteReaderAsync(
            IRequestInfo requestInfo, 
            OracleConnection oracleConnection, 
            string sql,
            OracleDynamicParameters? parameters = null, 
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15, 
            IDbTransaction? transaction = null)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                var reader = await oracleConnection.ExecuteReaderAsync(
                    sql: sql,
                    param: parameters,
                    commandType: commandType,
                    commandTimeout: commandTimeout,
                    transaction: transaction);

                LogSuccess(requestInfo, sql, parameters, stopwatch);

                return reader;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, sql, parameters, stopwatch);
            }
        }

        public async Task<T> GetAsync<T>(
            IRequestInfo requestInfo, 
            OracleConnectionNames oracleConnectionName, 
            string sql, 
            DynamicParameters? parameters, 
            CommandType commandType = CommandType.StoredProcedure, 
            int commandTimeout = 15, 
            IDbTransaction? transaction = null)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using var connection = new OracleConnection(GetConnectionString(oracleConnectionName));
                await connection.OpenAsync();

                var items = await connection.QueryAsync<T>(
                    sql: sql,
                    param: parameters,
                    commandType: commandType,
                    commandTimeout: commandTimeout,
                    transaction: transaction);

                LogSuccess(requestInfo, sql, parameters, stopwatch);

                return items.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, sql, parameters, stopwatch);
            }
        }

        public async Task<DataTable> GetDatatableAsync(
            IRequestInfo requestInfo, 
            OracleConnectionNames oracleConnectionName,
            string sql, 
            DynamicParameters? parameters = null, 
            CommandType commandType = CommandType.StoredProcedure, 
            int commandTimeout = 15)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using var connection = new OracleConnection(GetConnectionString(oracleConnectionName));
                await connection.OpenAsync();

                using var command = new OracleCommand(sql, connection);
                command.CommandTimeout = commandTimeout;
                command.CommandType = commandType;

                foreach (var paramName in parameters.ParameterNames)
                    command.Parameters.Add(new OracleParameter(paramName, parameters.Get<object>(paramName) ?? DBNull.Value));

                var dataTable = new DataTable();
                dataTable.Load(await command.ExecuteReaderAsync());

                LogSuccess(requestInfo, sql, parameters, stopwatch);

                return dataTable;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, sql, parameters, stopwatch);
            }
        }

        public async Task<T> GetSingleValueAsync<T>(
            IRequestInfo requestInfo, 
            OracleConnectionNames oracleConnectionName, 
            string sql, 
            DynamicParameters? parameters = null, 
            CommandType commandType = CommandType.StoredProcedure, 
            int commandTimeout = 15, 
            IDbTransaction? transaction = null)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using var connection = new OracleConnection(GetConnectionString(oracleConnectionName));
                await connection.OpenAsync();

                var ret = await connection.ExecuteScalarAsync<T>(
                    sql: sql,
                    param: parameters,
                    commandType: commandType,
                    commandTimeout: commandTimeout,
                    transaction: transaction);

                LogSuccess(requestInfo, sql, parameters, stopwatch);

                return ret;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, sql, parameters, stopwatch);
            }
        }

        public async Task<List<T>> ListAsync<T>(
            IRequestInfo requestInfo,
            OracleConnectionNames oracleConnectionName,
            string sql,
            DynamicParameters? parameters,
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15,
            IDbTransaction? transaction = null)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                using var connection = new OracleConnection(GetConnectionString(oracleConnectionName));
                await connection.OpenAsync();

                var items = await connection.QueryAsync<T>(
                    sql: sql,
                    param: parameters,
                    commandType: commandType,
                    commandTimeout: commandTimeout,
                    transaction: transaction);

                LogSuccess(requestInfo, sql, parameters, stopwatch);

                return items.ToList();
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, sql, parameters, stopwatch);
            }
        }

        #region Auxiliar Methods

        public string GetConnectionString(OracleConnectionNames oracleConnectionName)
        {
            var databaseKeyString = $"OracleSettings:{oracleConnectionName}";

            var databaseConnectionString = configuration.GetSection(databaseKeyString)?.Value;
            if (string.IsNullOrEmpty(databaseConnectionString))
                throw new Exception($"ConnectionString não configurado para Database {oracleConnectionName}");

            return databaseConnectionString;
        }

        private void LogSuccess(IRequestInfo requestInfo, string sql, DynamicParameters parameters, Stopwatch stopwatch)
        {
            var pametersText = $"parameters: {GetParametersText(parameters)}";
            customLogger.LogDbExecution(requestInfo, $"SQL Execute {sql}", pametersText, stopwatch.ElapsedMilliseconds);
        }

        private void LogSuccess(IRequestInfo requestInfo, string sql, OracleDynamicParameters parameters, Stopwatch stopwatch)
        {
            var pametersText = $"parameters: {GetParametersText(parameters)}";
            customLogger.LogDbExecution(requestInfo, $"SQL Execute {sql}", pametersText, stopwatch.ElapsedMilliseconds);
        }

        private Exception LogError(IRequestInfo requestInfo, Exception ex, string sql, DynamicParameters parameters, Stopwatch stopwatch)
        {
            string errorMessage = $"SQL Error -- {sql} -- Error: {ex.Message}";
            customLogger.LogError(requestInfo, errorMessage, ex, $"parameters: {GetParametersText(parameters)}", stopwatch.ElapsedMilliseconds);
            return new InvalidOperationException(errorMessage, ex);
        }

        private Exception LogError(IRequestInfo requestInfo, Exception ex, string sql, OracleDynamicParameters parameters, Stopwatch stopwatch)
        {
            string errorMessage = $"SQL Error -- {sql} -- Error: {ex.Message}";
            customLogger.LogError(requestInfo, errorMessage, ex, $"parameters: {GetParametersText(parameters)}", stopwatch.ElapsedMilliseconds);
            return new InvalidOperationException(errorMessage, ex);
        }

        private static string GetParametersText(DynamicParameters parameters)
        {
            if (parameters == null)
                return string.Empty;

            return string.Join(", ", parameters.ParameterNames.Select(a => $"{a}={parameters.Get<object>(a)}"));
        }

        private static string GetParametersText(OracleDynamicParameters parameters)
        {
            if (parameters == null)
                return string.Empty;

            return string.Join(", ", parameters.ParameterNames.Select(a => $"{a}={parameters.Get<object>(a)}"));
        }

        public string GetQueryFromFile(string queryDirectory, string fileName)
        {
            var filePath = Path.Combine(AppContext.BaseDirectory, "Queries", queryDirectory, fileName);
            if (!File.Exists(filePath))
            {
                System.Diagnostics.Debugger.Break(); //TIP: Não esqueça de marcar os arquivos .sql com a propriedade "Copy if newer" ao incluir novos na pasta Queries
                throw new ArgumentNullException($"File not found: {filePath}");
            }

            using var streamReader = new StreamReader(filePath);
            string query = streamReader.ReadToEnd();

            return query;
        }
        #endregion
    }
}