﻿using Plataforma.Oms.Estoque.Domain.Aggregates;

namespace Plataforma.Oms.Estoque.Infrastructure.Data.Oracle.Models
{
    public class InventoryProductModel
    {
        public string storeReference { get; set; }
        public string upsert { get; set; }
        public string othersOutOfStock { get; set; }
        public string Name { get; set; }
        public string sku { get; set; }
        public string photosUrls { get; set; }
        public string unit { get; set; }
        public string MyProperty { get; set; }
        public string price { get; set; }
        public string clickMultiplier { get; set; }
        public string specialMaxQty { get; set; }
        public string showSubUnit { get; set; }
        public string specialPrice { get; set; }
        public string subQty { get; set; }
        public string subUnit { get; set; }
        public string maxQty { get; set; }
        public long stock { get; set; }
        public string EAN { get; set; }
        public string boost { get; set; }
        public string location { get; set; }
        public string search_keywords { get; set; }
        public string brand { get; set; }
        public string slug { get; set; }
        public string categoryReference1 { get; set; }
        public string categoryReference2 { get; set; }
        public string categoryReference3 { get; set; }
        public string format { get; set; }
        public string equivalence { get; set; }
        public string unitEquivalence { get; set; }
        public DateTime dt_cadastro { get; set; }
        public DateTime dt_alteracao { get; set; }

        public InventoryProduct ConvertToDomain()
        {
            return new InventoryProduct(
                storeReference,
                upsert,
                othersOutOfStock,
                Name,
                sku,
                photosUrls,
                unit,
                MyProperty,
                price,
                clickMultiplier,
                specialMaxQty,
                showSubUnit,
                specialPrice,
                subQty,
                subUnit,
                maxQty,
                stock,
                EAN,
                boost,
                location,
                search_keywords,
                brand,
                slug,
                categoryReference1,
                categoryReference2,
                categoryReference3,
                format,
                equivalence,
                unitEquivalence,
                dt_cadastro,
                dt_alteracao
                );
        }
    }
}
