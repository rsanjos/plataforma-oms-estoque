﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.Core.Events;
using Plataforma.Oms.Estoque.CrossCutting.Domain;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.Infrastructure.Data.Mongo.Conventions;
using Plataforma.Oms.Estoque.Infrastructure.Interfaces;
using System.Diagnostics;
using System.Linq.Expressions;

namespace Plataforma.Oms.Estoque.Infrastructure.Data.Mongo
{
    public class MongoDataAccess : IMongoDataAccess
    {
        private readonly ICustomLogger customLogger;
        private readonly IMongoSettings mongoSettings;
        private readonly IMongoDatabase database;

        private string lastCommand = null;

        #region Construtores

        static MongoDataAccess()
        {
            SetMongoConfigurations();
        }

        public MongoDataAccess(ICustomLogger customLogger, IMongoSettings mongoSettings)
        {
            this.customLogger = customLogger ?? throw new ArgumentNullException(nameof(customLogger));
            this.mongoSettings = mongoSettings ?? throw new ArgumentNullException(nameof(mongoSettings));

            database = GetDatabase();

            var pack = new ConventionPack
            {
                new CamelCaseElementNameConvention()
            };
            ConventionRegistry.Register("camel case", pack, t => true);
        }

        #endregion

        public async Task<TDocument> GetAsync<TDocument>(IRequestInfo requestInfo, string collectionName, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "GetAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                var result = await collection.Find(filter).FirstOrDefaultAsync();

                LogSuccess(requestInfo, collectionName, operation, stopwatch);

                return result;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        public async Task<List<TDocument>> ListAsync<TDocument>(IRequestInfo requestInfo, string collectionName, FilterDefinition<TDocument> filter, FindOptions<TDocument>? options = null)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "ListAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                var listCursor = await collection.FindAsync(filter, options);
                var result = await listCursor.ToListAsync();

                LogSuccess(requestInfo, collectionName, operation, stopwatch);

                return result;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        public async Task<PagedList<TDocument>> GetPagedListAsync<TDocument>(IRequestInfo requestInfo, string collectionName, int page, int pageSize,
            FilterDefinition<TDocument> filter, FindOptions<TDocument>? options = null)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "GetPagedListAsync";
            try
            {
                if (options == null)
                    options = new FindOptions<TDocument>();

                options.Limit = pageSize;
                options.Skip = (page - 1) * pageSize;

                var collection = GetCollection<TDocument>(collectionName);
                var totalItems = await collection.CountDocumentsAsync(filter);
                var listCursor = await collection.FindAsync(filter, options);
                var result = await listCursor.ToListAsync();

                LogSuccess(requestInfo, collectionName, operation, stopwatch);

                var pagedList = new PagedList<TDocument>(new Pagination(page, pageSize, (int)totalItems), result);
                return pagedList;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        public async Task<TDocument> GetAndUpdateAsync<TDocument>(IRequestInfo requestInfo, string collectionName, Expression<Func<TDocument, bool>> filter,
            UpdateDefinition<TDocument> update, FindOneAndUpdateOptions<TDocument>? options = null)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "GetAndUpdateAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                var result = await collection.FindOneAndUpdateAsync(filter, update, options);

                LogSuccess(requestInfo, collectionName, operation, stopwatch);

                return result;
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        public async Task InsertAsync<TDocument>(IRequestInfo requestInfo, string collectionName, TDocument document)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "InsertAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.InsertOneAsync(document);

                LogSuccess(requestInfo, collectionName, operation, stopwatch);
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        public async Task UpdateAsync<TDocument>(IRequestInfo requestInfo, string collectionName, TDocument document, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "UpdateAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.ReplaceOneAsync(filter, document, new ReplaceOptions { IsUpsert = false });

                LogSuccess(requestInfo, collectionName, operation, stopwatch);
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        public async Task InsertOrUpdateAsync<TDocument>(IRequestInfo requestInfo, string collectionName, TDocument document, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "InsertOrUpdateAsync";
            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.ReplaceOneAsync(filter, document, new ReplaceOptions { IsUpsert = true });

                LogSuccess(requestInfo, collectionName, operation, stopwatch);
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        public async Task DeleteOneAsync<TDocument>(IRequestInfo requestInfo, string collectionName, FilterDefinition<TDocument> filter)
        {
            var stopwatch = Stopwatch.StartNew();
            var operation = "DeleteOneAsync";

            try
            {
                var collection = GetCollection<TDocument>(collectionName);
                await collection.DeleteOneAsync(filter);
                LogSuccess(requestInfo, collectionName, operation, stopwatch);
            }
            catch (Exception ex)
            {
                throw LogError(requestInfo, ex, collectionName, operation, stopwatch);
            }
        }

        #region Mongo Configurations

        private static void SetMongoConfigurations()
        {
            var pack = new ConventionPack
            {
                new GuidAsStringRepresentationConvention()
            };

            ConventionRegistry.Register(
                "GUIDs as strings Conventions",
                pack,
                type => type.Namespace.StartsWith(typeof(MongoDataAccess).Namespace));
        }

        #endregion

        #region Auxiliar Methods

        public IMongoDatabase GetDatabase()
        {
            var mongoConnectionUrl = new MongoUrl(mongoSettings.ConnectionString);
            var mongoClientSettings = MongoClientSettings.FromUrl(mongoConnectionUrl);
            mongoClientSettings.ClusterConfigurator = cb => {
                cb.Subscribe<CommandStartedEvent>(e => {
                    lastCommand = $"{e.CommandName} - {e.Command.ToJson()}";
                });
            };

            var client = new MongoClient(mongoClientSettings);
            return client.GetDatabase(mongoSettings.Database);
        }

        private void LogSuccess(IRequestInfo requestInfo, string collectionName, string operation, Stopwatch stopwatch)
        {
            customLogger.LogDbExecution(requestInfo, $"[mongo] {collectionName}.{operation}", lastCommand, stopwatch.ElapsedMilliseconds);
        }

        private Exception LogError(IRequestInfo requestInfo, Exception ex, string collectionName, string operation, Stopwatch stopwatch)
        {
            string errorMessage = $"Error executing command in mongo -- Collection: {collectionName}.{operation} -- Error: {ex.Message}";
            customLogger.LogError(requestInfo, errorMessage, ex, lastCommand, stopwatch.ElapsedMilliseconds);
            return new InvalidOperationException(errorMessage, ex);
        }

        private IMongoCollection<TDocument> GetCollection<TDocument>(string collectionName)
        {
            return database.GetCollection<TDocument>(collectionName);
        }

        #endregion
    }
}
