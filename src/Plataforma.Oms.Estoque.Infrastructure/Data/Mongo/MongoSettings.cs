﻿using Microsoft.Extensions.Configuration;
using Plataforma.Oms.Estoque.Infrastructure.Interfaces;

namespace Plataforma.Oms.Estoque.Infrastructure.Data.Mongo
{
    public class MongoSettings : IMongoSettings
    {
        private readonly IConfiguration configuration;
        private readonly IConfiguration mongoSettings;

        public string ConnectionString => mongoSettings.GetSection("ConnectionString").Value;
        public string Database => mongoSettings.GetSection("Database").Value;

        public MongoSettings(IConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            mongoSettings = configuration.GetSection("MongoSettings") ?? throw new ArgumentNullException(nameof(MongoSettings), "MongoSettings section is not defined in configuration file.");
        }

        public string GetCollectionName(MongoCollections mongoCollection, string tenantId)
        {
            var configName = $"MongoSettings:Collection{mongoCollection}Name";

            var section = configuration.GetSection(configName);

            if (section == null || string.IsNullOrEmpty(section.Value))
                throw new InvalidOperationException($"key \"{configName}\" not found in configuration file");

            var value = section.Value;
            if (value.Contains("{tenantId}"))
            {
                if (string.IsNullOrEmpty(tenantId))
                    throw new System.InvalidOperationException($"Error getting collection Name for \"{mongoCollection}\", tenantId is empty");

                value = value.Replace("{tenantId}", tenantId);
            }

            return value;
        }
    }
}