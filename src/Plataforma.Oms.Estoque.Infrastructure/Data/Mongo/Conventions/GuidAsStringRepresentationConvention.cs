﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;

namespace Plataforma.Oms.Estoque.Infrastructure.Data.Mongo.Conventions
{
    public class GuidAsStringRepresentationConvention : ConventionBase, IMemberMapConvention
    {
        //ref: https://kevsoft.net/2020/06/25/storing-guids-as-strings-in-mongodb-with-csharp.html

        public void Apply(BsonMemberMap memberMap)
        {
            if (memberMap.MemberType == typeof(Guid))
            {
                memberMap.SetSerializer(
                    new GuidSerializer(BsonType.String));
            }
            else if (memberMap.MemberType == typeof(Guid?))
            {
                memberMap.SetSerializer(
                    new NullableSerializer<Guid>(new GuidSerializer(BsonType.String)));
            }
        }
    }
}
