﻿using AutoMapper;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.Domain.Aggregates;
using Plataforma.Oms.Estoque.Domain.Repositories;
using Plataforma.Oms.Estoque.Infrastructure.Data.Oracle;
using Plataforma.Oms.Estoque.Infrastructure.Data.Oracle.Models;
using Plataforma.Oms.Estoque.Infrastructure.Interfaces;
using System.Data;

namespace Plataforma.Oms.Estoque.Infrastructure.Repositories
{
    public class InventoryRepository : IInventoryRepository
    {
        public IOracleDataAccess oracleDataAccess;

        public InventoryRepository(
            IOracleDataAccess oracleDataAccess)
        {
            this.oracleDataAccess = oracleDataAccess ?? throw new ArgumentException(nameof(oracleDataAccess));
        }

        public async Task<InventoryProduct> GetInventoryAsync(IRequestInfo requestInfo, string storeNumber, string productNumber)
        {
            var oracleConnection = new OracleConnection(oracleDataAccess.GetConnectionString(OracleConnectionNames.RedeOba));

            var sql = "CONSINCO.PKG_INSTALEAP.P_BUSCAPRODUTO";
            var parameters = new OracleDynamicParameters();
            parameters.Add("pr_seqproduto", productNumber);
            parameters.Add("pr_nroempresa", storeNumber);
            parameters.Add("pr_dataBase", null);
            parameters.Add("r_result", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            var reader = await this.oracleDataAccess.ExecuteReaderAsync(requestInfo, oracleConnection, sql, parameters);
            var parser = reader.GetRowParser<InventoryProductModel>(typeof(InventoryProductModel));

            if (reader.Read())
            {
                var result = parser(reader);
                return result.ConvertToDomain();
            }

            return null;
        }
    }
}