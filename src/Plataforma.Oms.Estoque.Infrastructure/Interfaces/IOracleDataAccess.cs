﻿using Dapper;
using Oracle.ManagedDataAccess.Client;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using Plataforma.Oms.Estoque.Infrastructure.Data.Oracle;
using System.Data;
using System.Data.Common;

namespace Plataforma.Oms.Estoque.Infrastructure.Interfaces
{
    public interface IOracleDataAccess
    {
        string GetConnectionString(OracleConnectionNames oracleConnectionName);

        Task<List<T>> ListAsync<T>(
            IRequestInfo requestInfo,
            OracleConnectionNames oracleConnectionName,
            string sql,
            DynamicParameters? parameters,
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15,
            IDbTransaction? transaction = null);

        Task<T> GetAsync<T>(
            IRequestInfo requestInfo,
            OracleConnectionNames oracleConnectionName,
            string sql,
            DynamicParameters? parameters,
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15,
            IDbTransaction? transaction = null);

        Task<T> GetSingleValueAsync<T>(
            IRequestInfo requestInfo,
            OracleConnectionNames oracleConnectionName,
            string sql,
            DynamicParameters? parameters = null,
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15,
            IDbTransaction? transaction = null);

        Task<int> ExecuteAsync(
            IRequestInfo requestInfo,
            OracleConnectionNames oracleConnectionName,
            string sql,
            DynamicParameters? parameters = null,
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15,
            IDbTransaction? transaction = null);

        Task<DbDataReader> ExecuteReaderAsync(
            IRequestInfo requestInfo,
            OracleConnection sqlConnection,
            string sql,
            OracleDynamicParameters parameters = null,
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15,
            IDbTransaction transaction = null);

        Task<DataTable> GetDatatableAsync(
            IRequestInfo requestInfo,
            OracleConnectionNames oracleConnectionName,
            string sql,
            DynamicParameters parameters = null,
            CommandType commandType = CommandType.StoredProcedure,
            int commandTimeout = 15);

        string GetQueryFromFile(string queryDirectory, string fileName);
    }
}