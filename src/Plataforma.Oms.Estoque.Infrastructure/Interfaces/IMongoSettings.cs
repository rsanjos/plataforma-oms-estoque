﻿using Plataforma.Oms.Estoque.Infrastructure.Data.Mongo;

namespace Plataforma.Oms.Estoque.Infrastructure.Interfaces
{
    public interface IMongoSettings
    {
        string ConnectionString { get; }
        string Database { get; }
        string GetCollectionName(MongoCollections mongoCollection, string tenantId);
    }
}