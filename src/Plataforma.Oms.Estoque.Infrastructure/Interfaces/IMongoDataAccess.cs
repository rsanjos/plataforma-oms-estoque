﻿using MongoDB.Driver;
using Plataforma.Oms.Estoque.CrossCutting.Domain;
using Plataforma.Oms.Estoque.CrossCutting.Logging;
using System.Linq.Expressions;

namespace Plataforma.Oms.Estoque.Infrastructure.Interfaces
{
    public interface IMongoDataAccess
    {
        Task<TDocument> GetAsync<TDocument>(IRequestInfo requestInfo, string collectionName, FilterDefinition<TDocument> filter);
        Task<List<TDocument>> ListAsync<TDocument>(IRequestInfo requestInfo, string collectionName, FilterDefinition<TDocument> query, FindOptions<TDocument> options = null);
        Task<PagedList<TDocument>> GetPagedListAsync<TDocument>(IRequestInfo requestInfo, string collectionName, int page, int pageSize, FilterDefinition<TDocument> filter, FindOptions<TDocument> options = null);
        Task<TDocument> GetAndUpdateAsync<TDocument>(IRequestInfo requestInfo, string collectionName, Expression<Func<TDocument, bool>> filter, UpdateDefinition<TDocument> update, FindOneAndUpdateOptions<TDocument> options = null);
        Task InsertAsync<TDocument>(IRequestInfo requestInfo, string collectionName, TDocument document);
        Task UpdateAsync<TDocument>(IRequestInfo requestInfo, string collectionName, TDocument document, FilterDefinition<TDocument> filter);
        Task InsertOrUpdateAsync<TDocument>(IRequestInfo requestInfo, string collectionName, TDocument document, FilterDefinition<TDocument> filter);
        Task DeleteOneAsync<TDocument>(IRequestInfo requestInfo, string collectionName, FilterDefinition<TDocument> filter);
        IMongoDatabase GetDatabase();
    }
}