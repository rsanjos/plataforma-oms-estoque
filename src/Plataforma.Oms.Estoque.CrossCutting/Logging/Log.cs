﻿namespace Plataforma.Oms.Estoque.CrossCutting.Logging
{
    public class Log : ILog
    {
        public string Application { get; set; }
        public string MessageType { get; set; }
        public string Message { get; set; }
        public int? ElapsedTimeMilliseconds { get; set; }
        public int? HttpStatusCode { get; set; }
        public string Details { get; set; }
        public IRequestInfo RequestInfo { get; set; }
        public string Exception { get; set; }
        public string StackTrace { get; set; }
        public string SystemInformation { get; set; }
        public Log(string application, string messageType)
        {
            this.Application = application;
            this.MessageType = messageType;
        }
        public override string ToString()
        {
            return $"{this.MessageType} {this.Message}";
        }
    }
}