﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.CrossCutting.Logging
{
    public class RequestInfo : IRequestInfo
    {
        public Guid CorrelationId { get; set; }
        public ApplicationEnvironments ApplicationEnvironment { get; set; }
        public string Url { get; set; }
        public string HttpVerb { get; set; }
        public string Ip { get; set; }
        public string UserAgent { get; set; }
        public string MachineName { get; set; }
        public string TenantId { get; set; }

        public RequestInfo(Guid correlationId, ApplicationEnvironments applicationEnvironment,
            string url, string httpVerb, string ip, string userAgent, string machineName,
            string tenantId)
        {
            CorrelationId = correlationId;
            ApplicationEnvironment = applicationEnvironment;
            Url = url;
            HttpVerb = httpVerb;
            Ip = ip;
            UserAgent = userAgent;
            MachineName = machineName;
            TenantId = tenantId;
        }
    }
}