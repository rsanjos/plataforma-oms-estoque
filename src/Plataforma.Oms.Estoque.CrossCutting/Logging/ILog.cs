﻿namespace Plataforma.Oms.Estoque.CrossCutting.Logging
{
    public interface ILog
    {
        string Application { get; set; }
        string MessageType { get; set; }
        string Message { get; set; }
        int? ElapsedTimeMilliseconds { get; set; }
        int? HttpStatusCode { get; set; }
        string Details { get; set; }
        IRequestInfo RequestInfo { get; set; }
        string Exception { get; set; }
        string StackTrace { get; set; }
        string SystemInformation { get; set; }
    }
}