﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plataforma.Oms.Estoque.CrossCutting.Logging
{
    public interface ICustomLogger
    {
        string ApplicationName { get; set; }
        void LogRequestStart(IRequestInfo requestInfo, string? requestContent = null);
        void LogRequestEnd(IRequestInfo requestInfo, int? httpStatusCode, double? elapsedTimeMilliseconds, string responseContent = null);
        void LogDebug(IRequestInfo requestInfo, string message, string? details = null, double? elapsedTimeMilliseconds = null);
        void LogInformation(IRequestInfo requestInfo, string message, string? details = null, double? elapsedTimeMilliseconds = null);
        void LogError(IRequestInfo requestInfo, string message, Exception? exception = null, string? details = null, double? elapsedTimeMilliseconds = null);
        void LogExternalCall(IRequestInfo requestInfo, string message, string url, string request, string response, int? httpStatusCode, double? elapsedTimeMilliseconds = null, Exception? exception = null);
        void LogEventProduced(IRequestInfo requestInfo, string topicName, string eventIdentification, string? details = null);
        void LogEventReceived(IRequestInfo requestInfo, string topicName, string eventIdentification, string? details = null);
        void LogEventProcessedWithSuccess(IRequestInfo requestInfo, string topicName, string eventIdentification, string? details = null, double? elapsedTimeMilliseconds = null);
        void LogEventProcessedWithError(IRequestInfo requestInfo, string topicName, string eventIdentification, string errorMessage, Exception? exception = null, string? details = null, double? elapsedTimeMilliseconds = null);
        void LogDbExecution(IRequestInfo requestInfo, string message, string? details = null, double? elapsedTimeMilliseconds = null);
        void LogCacheGet(IRequestInfo requestInfo, string message, string? details = null, double? elapsedTimeMilliseconds = null);
        void LogCacheSet(IRequestInfo requestInfo, string message, string? details = null, double? elapsedTimeMilliseconds = null);
        void Log(ILog log, CustomLogLevels customLogLevel);
    }
}
