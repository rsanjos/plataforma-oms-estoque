﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.CrossCutting.Logging
{
    public interface IRequestInfo
    {
        Guid CorrelationId { get; set; }
        ApplicationEnvironments ApplicationEnvironment { get; set; }
        string Url { get; set; }
        string HttpVerb { get; set; }
        string Ip { get; set; }
        string UserAgent { get; set; }
        string MachineName { get; set; }
        string TenantId { get; set; }
    }
}