﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;
using Plataforma.Oms.Estoque.CrossCutting.Monitor;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Plataforma.Oms.Estoque.CrossCutting.Logging
{
    public abstract class CustomBaseLogger : ICustomLogger
    {
        public string ApplicationName { get; set; }
        private readonly JsonSerializerOptions jsonSerializerOptions;

        public CustomBaseLogger(string applicationName)
        {
            this.ApplicationName = applicationName;

            this.jsonSerializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web)
            {
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingDefault
            };
            jsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
        }

        public void LogRequestStart(IRequestInfo requestInfo, string requestContent = null)
        {
            var log = new Log(this.ApplicationName, "request-start")
            {
                Message = $"Request Start {requestInfo.Url}",
                Exception = null,
                Details = null,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);

            if (!string.IsNullOrEmpty(requestContent))
            {
                log.MessageType = "request-start-payload";
                log.Details = requestContent;
                LogInternal(log, CustomLogLevels.Debug);
            }
        }

        public void LogRequestEnd(IRequestInfo requestInfo, int? httpStatusCode, double? elapsedTimeMilliseconds, string responseContent = null)
        {
            var log = new Log(this.ApplicationName, "request-end")
            {
                Message = $"Request End {requestInfo.Url}",
                Exception = null,
                Details = null,
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = httpStatusCode
            };

            LogInternal(log, CustomLogLevels.Information);

            if (!string.IsNullOrEmpty(responseContent))
            {
                log.MessageType = "request-end-payload";
                log.Details = responseContent;
                LogInternal(log, CustomLogLevels.Debug);
            }
        }

        public void LogDebug(IRequestInfo requestInfo, string message, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "debug")
            {
                Message = message,
                Exception = null,
                Details = details,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Debug);
        }

        public void LogInformation(IRequestInfo requestInfo, string message, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "message")
            {
                Message = message,
                Exception = null,
                Details = details,
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }

        public void LogError(IRequestInfo requestInfo, string message, Exception exception = null, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "error")
            {
                Message = message,
                Exception = (exception != null ? GetFullExceptionMessage(exception) : null),
                Details = details,
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null,
                SystemInformation = GetSystemInformation()
            };

            LogInternal(log, CustomLogLevels.Error);
        }

        public void LogExternalCall(IRequestInfo requestInfo, string message, string url, string request, string response, int? httpStatusCode, double? elapsedTimeMilliseconds = null, Exception exception = null)
        {
            var log = new Log(this.ApplicationName, "external-call")
            {
                Message = message,
                Exception = (exception != null ? GetFullExceptionMessage(exception) : null),
                Details = $"request: {request ?? "null"}\r\nresponse: {response ?? "null"}",
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = httpStatusCode
            };

            LogInternal(log, CustomLogLevels.Information);
        }

        public void LogEventProduced(IRequestInfo requestInfo, string topicName, string eventIdentification, string details = null)
        {
            var log = new Log(this.ApplicationName, "event-produced")
            {
                Message = $"Event produced {topicName}",
                Exception = null,
                Details = $"Event produced {topicName} -- EventIdentification: {eventIdentification}{(details != null ? $" -- Details: {details}" : null)}",
                ElapsedTimeMilliseconds = null,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }

        public void LogEventReceived(IRequestInfo requestInfo, string topicName, string eventIdentification, string details = null)
        {
            var log = new Log(this.ApplicationName, "event-received")
            {
                Message = $"Event received {topicName}",
                Exception = null,
                Details = $"Event produced {topicName} -- EventIdentification: {eventIdentification}{(details != null ? $" -- Details: {details}" : null)}",
                ElapsedTimeMilliseconds = null,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }

        public void LogEventProcessedWithSuccess(IRequestInfo requestInfo, string topicName, string eventIdentification, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "event-processed-success")
            {
                Message = $"Event processed with success {topicName}",
                Exception = null,
                Details = $"Event processed with error {topicName} -- EventIdentification: {eventIdentification}{(details != null ? $" -- Details: {details}" : null)}",
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }

        public void LogEventProcessedWithError(IRequestInfo requestInfo, string topicName, string eventIdentification, string errorMessage, Exception exception = null, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "event-processed-error")
            {
                Message = $"Event processed with error {topicName} -- Error: {errorMessage}",
                Exception = (exception != null ? GetFullExceptionMessage(exception) : null),
                Details = $"Event processed with error {topicName} -- EventIdentification: {eventIdentification} -- Error: {errorMessage}{(details != null ? $" -- Details: {details}" : null)}",
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Error);
        }

        public void LogDbExecution(IRequestInfo requestInfo, string message, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "db-exec")
            {
                Message = message,
                Exception = null,
                Details = details,
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }

        public void LogCacheGet(IRequestInfo requestInfo, string message, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "cache-get")
            {
                Message = message,
                Exception = null,
                Details = details,
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }

        public void LogCacheSet(IRequestInfo requestInfo, string message, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "cache-set")
            {
                Message = message,
                Exception = null,
                Details = details,
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }


        public void LogCacheDelete(IRequestInfo requestInfo, string message, string details = null, double? elapsedTimeMilliseconds = null)
        {
            var log = new Log(this.ApplicationName, "cache-delete")
            {
                Message = message,
                Exception = null,
                Details = details,
                ElapsedTimeMilliseconds = (int?)elapsedTimeMilliseconds,
                RequestInfo = requestInfo,
                HttpStatusCode = null
            };

            LogInternal(log, CustomLogLevels.Information);
        }


        public abstract void Log(ILog log, CustomLogLevels customLogLevel);

        #region Auxiliar

        private void LogInternal(Log log, CustomLogLevels customLogLevel)
        {
            log.StackTrace = GetStackTrace();
            Log(log, customLogLevel);
        }

        private static string GetFullExceptionMessage(System.Exception ex)
        {
            if (ex == null)
                return null;

            var sb = new System.Text.StringBuilder();

            sb.AppendFormat("Message: {0}\r\n", ex.Message);

            var type = ex.GetType();
            sb.AppendFormat("Type: {0} ({1})\r\n", type.FullName, type.Assembly.FullName);

            if (!string.IsNullOrEmpty(ex.Source))
                sb.AppendFormat("Source: {0}\r\n", ex.Source);

            if (ex.TargetSite != null)
                sb.AppendFormat("TargetSite: {0}\r\n", ex.TargetSite);

            if (ex is System.ComponentModel.Win32Exception w32ex)
                sb.AppendFormat("ErrorCode: {0}\r\n", w32ex.ErrorCode);

            if (!string.IsNullOrEmpty(ex.HelpLink))
                sb.AppendFormat("HelpLink: {0}\r\n", ex.HelpLink);

            if (ex.Data != null && ex.Data.Count > 0)
            {
                sb.Append("Data:\r\n");
                foreach (System.Collections.DictionaryEntry item in ex.Data)
                    sb.AppendFormat("\t{0}\t{1}\r\n", item.Key, item.Value);
            }

            if (!string.IsNullOrEmpty(ex.StackTrace))
                sb.AppendFormat("StackTrace:\r\n---------------------------\r\n{0}\r\n---------------------------\r\n", ex.StackTrace);

            if (ex.InnerException != null)
            {
                sb.Append("\r\n>> INNER EXCEPTION:\r\n");
                sb.Append(GetFullExceptionMessage(ex.InnerException));
            }

            return sb.ToString();
        }

        private static string GetStackTrace(int linesLimit = 3)
        {
            var stackTrace = new System.Diagnostics.StackTrace();

            var stackTraceBaseText = System.Environment.StackTrace;
            var lines = stackTraceBaseText.Split(new string[] { "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries).Select(a => a.Trim()).ToList();

            if (lines.Count > 5) //Remove os métodos chamados nessa mesma classe: 1-GetStackTrace(), 2-get_StackTrace(), 3-Logger.GetStackTrace(), 4-Logger.SaveLog(), 5-Logger.WriteRequestLog()
                lines = lines.Skip(5).ToList();

            lines = lines.Where(a => !a.StartsWith("at Microsoft.") && !a.StartsWith("at System.")).ToList();

            if (lines.Count > linesLimit)
                lines = lines.Take(linesLimit).ToList();

            var text = string.Join("\r\n", lines);
            return text;
        }

        private string GetSystemInformation()
        {
            var systemInformation = SystemInformation.Get();
            return JsonSerializer.Serialize(systemInformation, jsonSerializerOptions);
        }

        #endregion
    }
}