﻿namespace Plataforma.Oms.Estoque.CrossCutting.Enum
{
    public enum ApplicationEnvironments
    {
        None = 0,
        Development = 1,
        Homolog = 2,
        Production = 3
    }
}