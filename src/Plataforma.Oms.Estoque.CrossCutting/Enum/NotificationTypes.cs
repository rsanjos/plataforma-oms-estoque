﻿namespace Plataforma.Oms.Estoque.CrossCutting.Enum
{
    public enum NotificationTypes
    {
        None = 0,
        Information = 1,
        InvalidConfiguration = 2,
        BadRequestError = 400,
        UnauthorizedError = 401,
        ForbiddenError = 403,
        NotFoundError = 404,
        ConflitError = 409,
        InternalServerError = 500,
        ServiceUnavailableError = 503,
        BadGateway = 502,
        GatewayTimeout = 504
    }
}