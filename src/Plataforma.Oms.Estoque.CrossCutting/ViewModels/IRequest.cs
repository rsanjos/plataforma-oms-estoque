﻿using Plataforma.Oms.Estoque.CrossCutting.Logging;

namespace Plataforma.Oms.Estoque.CrossCutting.ViewModels
{
    public interface IRequest
    {
        string TenantId { get; set; }
        IRequestInfo RequestInfo { get; set; }
    }
}
