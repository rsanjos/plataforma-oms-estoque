﻿using FluentValidation.Results;
using Plataforma.Oms.Estoque.CrossCutting.Domain;
using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.CrossCutting.ViewModels
{
    public interface IResponse
    {
        List<Notification> Notifications { get; }
        void AddNotification(NotificationTypes notificationType, string message);
        void AddNotification(Notification notification);
        void AddNotification(List<Notification> notifications);
        void AddFluentNotifications(ValidationResult result);
        bool HasErrors();
        string GetErrorMessage();
        void AddHeader(string key, string value);
        void AddCorrelationHeader(Guid correlationId);
        IDictionary<string, string> GetHeaders();
    }
}
