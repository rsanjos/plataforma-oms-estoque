﻿namespace Plataforma.Oms.Estoque.CrossCutting.ViewModels
{
    public interface IHeader
    {
        string Key { get; }
        string Value { get; }
    }
}
