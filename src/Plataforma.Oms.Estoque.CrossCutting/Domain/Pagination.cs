﻿namespace Plataforma.Oms.Estoque.CrossCutting.Domain
{
    public class Pagination
    {
        public int Page { get; private set; }

        public int PageSize { get; private set; }

        public int TotalItems { get; private set; }

        public int TotalPages { get; private set; }

        public Pagination(int page, int pageSize, int totalItems)
        {
            this.Page = page;
            this.PageSize = pageSize;
            this.TotalItems = totalItems;
            this.TotalPages = (totalItems > 0 ? ((totalItems - 1) / pageSize) + 1 : 0);
        }
    }
}
