﻿namespace Plataforma.Oms.Estoque.CrossCutting.Domain
{
    public class PagedList<T>
    {
        public Pagination Pagination { get; private set; }

        public List<T> Items { get; private set; }

        public PagedList(Pagination pagination, List<T> items)
        {
            this.Pagination = pagination;
            this.Items = items;
        }
    }
}