﻿using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.CrossCutting.Domain
{
    public class Notification
    {
        public NotificationTypes NotificationType { get; set; }
        public string Message { get; set; }

        public bool IsError() { return (this.NotificationType != NotificationTypes.None && this.NotificationType != NotificationTypes.Information); }

        public Notification()
        {

        }

        public Notification(NotificationTypes notificationType, string message)
        {
            this.NotificationType = notificationType;
            this.Message = message;
        }
    }
}