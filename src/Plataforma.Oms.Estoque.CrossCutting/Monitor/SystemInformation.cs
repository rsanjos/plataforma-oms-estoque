﻿using System.Diagnostics;

namespace Plataforma.Oms.Estoque.CrossCutting.Monitor
{
    public class SystemInformation
    {
        public static SystemInformation Get(bool forceFullCollection = false)
        {
            try
            {
                System.Threading.ThreadPool.GetMinThreads(out int minWorkerThreads, out int minAsynchronousThreads);
                System.Threading.ThreadPool.GetMaxThreads(out int maxWorkerThreads, out int maxAsyncronousThreads);
                System.Threading.ThreadPool.GetAvailableThreads(out int availableWorkerThreads, out int availableAsyncronousThreads);

                var currentProcess = System.Diagnostics.Process.GetCurrentProcess();

                var memoryInfo = System.GC.GetGCMemoryInfo();

                var info = new SystemInformation()
                {
                    MachineName = Environment.MachineName,
                    OSVersion = Environment.OSVersion.ToString(),
                    ThreadPool = new ThreadPoolInfo()
                    {
                        WorkerThreads = new ThreadItemPoolInfo()
                        {
                            Min = minWorkerThreads,
                            Max = maxWorkerThreads,
                            Available = availableWorkerThreads
                        },
                        AssyncronousThreads = new ThreadItemPoolInfo()
                        {
                            Min = minAsynchronousThreads,
                            Max = maxAsyncronousThreads,
                            Available = availableAsyncronousThreads
                        },
                        ThreadCount = System.Threading.ThreadPool.ThreadCount,
                        PendingWorkItemCount = System.Threading.ThreadPool.PendingWorkItemCount
                    },
                    GC = new GCInfo()
                    {
                        TotalAllocatedMemory = SizeSuffix(System.GC.GetTotalMemory(forceFullCollection)),
                        TotalAvailableMemory = SizeSuffix(memoryInfo.TotalAvailableMemoryBytes),
                        TotalCommitted = SizeSuffix(memoryInfo.TotalCommittedBytes)
                    },
                    Process = new ProcessInfo()
                    {
                        Id = currentProcess.Id,
                        ProcessName = currentProcess.ProcessName,
                        PrivateMemorySize64 = SizeSuffix(currentProcess.PrivateMemorySize64),
                        Threads = currentProcess.Threads.Count
                    }
                };

                return info;
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine($"ERROR in SystemInformation.Get() -- {ex.Message}");
                return null;
            }
        }

        public string MachineName { get; set; }
        public string OSVersion { get; set; }
        public ThreadPoolInfo ThreadPool { get; set; }
        public GCInfo GC { get; set; }
        public ProcessInfo Process { get; set; }

        public class ThreadPoolInfo
        {
            public ThreadItemPoolInfo WorkerThreads { get; set; }
            public ThreadItemPoolInfo AssyncronousThreads { get; set; }
            public int ThreadCount { get; set; }
            public long PendingWorkItemCount { get; set; }
        }

        public class ThreadItemPoolInfo
        {
            public int Min { get; set; }
            public int Max { get; set; }
            public int Available { get; set; }
        }

        public class GCInfo
        {
            public string TotalAllocatedMemory { get; set; }
            public string TotalAvailableMemory { get; set; }
            public string TotalCommitted { get; set; }
        }

        public class ProcessInfo
        {
            public int Id { get; set; }
            public string ProcessName { get; set; }
            public string PrivateMemorySize64 { get; set; }
            public int Threads { get; set; }
        }

        #region Auxiliar Methods

        static string SizeSuffix(long value, int decimalPlaces = 2)
        {
            var suffixies = new string[] { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException(nameof(decimalPlaces)); }
            if (value < 0) { return "-" + SizeSuffix(-value, decimalPlaces); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}", adjustedSize, suffixies[mag]);
        }

        #endregion
    }
}