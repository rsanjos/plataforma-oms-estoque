﻿using FluentValidation;
using Plataforma.Oms.Estoque.CrossCutting.Enum;

namespace Plataforma.Oms.Estoque.CrossCutting.Validators
{
    public static class ValidatorExtensions
    {
        #region ValidGuid

        public static IRuleBuilderOptions<T, TProperty> ValidGuid<T, TProperty>(
            this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.Must(x => IsValidGuid(x?.ToString())).WithMessage("Invalid GUID");
        }

        private static bool IsValidGuid(string text)
        {
            if (!Guid.TryParse(text, out Guid guid))
                return false;
            return (guid != Guid.Empty);
        }

        #endregion

        #region ValidTenantId

        public static IRuleBuilderOptions<T, TProperty> ValidTenantId<T, TProperty>(
            this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.Must(x => IsValidTenantId(x?.ToString())).WithMessage("Invalid TenentID");
        }

        private static bool IsValidTenantId(string text)
        {
            if (!System.Enum.TryParse(text, false, out Tenants value))
                return false;
            return (value != Tenants.None);
        }

        #endregion

        #region ValidCpfCnpj

        public static IRuleBuilderOptions<T, TProperty> ValidCpfCnpj<T, TProperty>(
            this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.Must(x => IsValidCpfCnpj(x?.ToString())).WithMessage("Invalid CpfCnpj");
        }

        private static bool IsValidCpfCnpj(string text)
        {
            return IsValidCpf(text) || IsValidCnpj(text);
        }

        private static bool IsValidCpf(string text)
        {
            return true; //TODO: Implementar
        }

        private static bool IsValidCnpj(string text)
        {
            return true; //TODO: Implementar
        }

        #endregion

        #region ValidDate

        public static IRuleBuilderOptions<T, TProperty> ValidDateTime<T, TProperty>(
            this IRuleBuilder<T, TProperty> ruleBuilder)
        {
            return ruleBuilder.Must(x => IsValidDateTime(x?.ToString())).WithMessage("Invalid GUID");
        }

        private static bool IsValidDateTime(string text)
        {
            return DateTime.TryParse(text, out DateTime guid);
        }

        #endregion
    }
}
